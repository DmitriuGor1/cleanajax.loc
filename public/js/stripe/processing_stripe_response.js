function processingStripeResponse (response) {
    if (response.success) {

        $('.stripe-response-fail').is(':visible')
        ? $('.stripe-response-fail').hide(1000)
        : null;

        $('.stripe-response').is(':hidden') 
        ? $(".stripe-response").show(2000)
        : null;

        return;
        
    } else {

        if ($(".stripe-response-fail" ).is(":hidden")) {
            $(".stripe-response-fail").show(2000);
            $('#stripe-fail').text(response.result.message);
        }
    }
}