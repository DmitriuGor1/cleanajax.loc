$(document).ready(function () {
    $( '.msg' ).hide(10400)
    $(".refund").click(function(e) {
        e.preventDefault();
            if (!confirm("Are You Sure to cancele order?")) {
            return false;
            }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax ({
            dataType : 'json',
            url : "/order/refundView",
        })
        .done( function (response) {

            if (response) {
                document.location.reload(true);
            }
        })
    });
});