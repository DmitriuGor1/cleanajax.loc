$(document).ready(function () {
    var submited = false;
    $(".pay_icon").click( function () { 
        if ($( ".stripe-pay" ).is(":visible")) {
            $(".stripe-pay").hide(3000);
        } else {
            $(".stripe-pay").show(3000);
        }
    });

    $(".btn-submit").click(function(e) {
        e.preventDefault();
        submited = false;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        form = $('#form_submit');
        let fd = new FormData(form[0]);
        // console.log(fd.getAll('name'));

        $.ajax ({
            url : "/order/store",
            method : 'post',
            contentType: false,
            processData: false,
            data : fd,
        }).done( function (response) {

            if (response) {
                submited = true;

                $('.invalid-feedback').text(response.errors).hide();
                $('.form-control').removeClass('is-invalid');

                $('#order_image').removeClass('fa fa-check');
                $('#order_image').removeClass('fa fa-times');

                $('#order_address').text(response.result.address);
                $('#order_flat_area').text(response.result.flat_area);
                $('#order_rooms').text(response.result.rooms);
                $('#order_bathroom').text(response.result.bathroom);
                $('#order_kitchen').text(response.result.kitchen);
                $('#order_refrigerator').text(response.result.refrigerator);
                $('#order_wardrobes').text(response.result.wardrobes);
                $('#order_animals').text(response.result.animals);
                $('#order_children').text(response.result.children);
                $('#order_adults').text(response.result.adults);
                $('#order_ammount').text(response.result.order_ammount);

                if (response.result.image) {
                    $('#order_image').addClass('fa fa-check').css('color', 'green');
                } else {
                    $('#order_image').addClass('fa fa-times').css('color', 'red');
                }

                $('.pay-methods').is(':hidden') 
                ? $(".pay-methods").show(2000)
                : null;
            }

        }).fail(function (jqXHR) {

            if (jqXHR.status == 422) {
                validationFunction(jqXHR.responseJSON);
            }else {
                msg = 'Uncaught Error.\n' + jqXHR.responseText;
            }
        });
    });
});

function validationFunction(response) {
    if (response.errors) {

        $('#inputName').addClass(' is-invalid');
        $('#error_inputName').text(response.errors.name).show();
        if (!response.errors.name) {
            validationHide('#error_inputName', '#inputName', response);
        }

        $('#inputSurname').addClass(' is-invalid');
        $('#error_inputSurname').text(response.errors.surname).show();
        if (!response.errors.surname) {
            validationHide('#error_inputSurname', '#inputSurname', response);
        }

        $('#inputPhone').addClass(' is-invalid');
        $('#error_inputPhone').text(response.errors.phone).show();
        if (!response.errors.phone) {
            validationHide('#error_inputPhone', '#inputPhone', response);
        }

        $('#inputEmail').addClass(' is-invalid');
        $('#error_inputEmail').text(response.errors.email).show();
        if (!response.errors.email) {
            validationHide('#error_inputEmail', '#inputEmail', response);
        } 

        $('#inputAddress').addClass(' is-invalid');
        $('#error_inputAddress').text(response.errors.address).show();
        if (!response.errors.address) {
            validationHide('#error_inputAddress', '#inputAddress', response);
        }

        $('#inputFlatArea').addClass(' is-invalid');
        $('#error_inputFlatArea').text(response.errors.flat_area).show();
        if (!response.errors.flat_area) {
            validationHide('#error_inputFlatArea', '#inputFlatArea', response);
        }
    }
}

function validationHide(id_error_field, id_input_field, response) {
    $(id_input_field).removeClass(' is-invalid');
    $(id_error_field).text(response.errors).hide();
}
