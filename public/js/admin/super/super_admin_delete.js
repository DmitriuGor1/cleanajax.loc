$(document).ready(function () {
    $( '.msg' ).hide(8400)
});

function myFunction(id) {

    if (!confirm("Are You Sure to delete admin")) {
        return false;
    }

    let super_delete_backend_url = '/admin/super/deleteUser' + '?user_id=' + id;
    event.preventDefault();
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax( {
        dataType : 'json',
        url: super_delete_backend_url,
    }).done( function (response) {

        document.location.reload(true);
    });
}