$(document).ready(function () {
    $(".btn-submit").click(function(e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        let name = $("input[name=name]").val();
        let email = $("input[name=email]").val();
        let password = $("input[name=password]").val();
        let password_confirmation = $("input[name=password_confirmation]").val();
        let id = $("input[name=id]").val();

        $.ajax ({
            serverSide : true,
            dataType : 'json',
            url : "/admin/super/storeUser" + "?user_id=" + id,
            method : 'post',
            data : {
                name : name,
                email : email,
                password : password,
                password_confirmation : password_confirmation,
            }
        }).done( function (response) {

            document.location.assign("/admin/super/admin");

        }).fail(function (jqXHR) {
            console.log(jqXHR);
            // Our error logic here

            if (jqXHR.status == 422) {
                validationFunction(jqXHR.responseJSON);
            }
        });
    });
});

function validationFunction(response) {
    if (response.errors) {

        $('#input_name_id').addClass(' is-invalid');
        $('#error_name').text(response.errors.name).show();
        if (!response.errors.name) {
            validationHide('#error_name', response);
            $('#input_name_id').removeClass(' is-invalid');
        }

        $('#input_email_id').addClass(' is-invalid');
        $('#error_email').text(response.errors.email).show();
        if (!response.errors.email) {
            validationHide('#error_email', response);
            $('#input_email_id').removeClass(' is-invalid');
        }

        $('#input_password_id').addClass(' is-invalid');
        $('#error_password').text(response.errors.password).show();
        if (!response.errors.password) {
            validationHide('#error_password', response);
            $('#input_password_id').removeClass(' is-invalid');
        }

        $('#input_password_confirmation_id').addClass(' is-invalid');
        $('#error_password_confirmation').text(response.errors.password_confirmation).show();
        if (!response.errors.password) {
            validationHide('#error_password_confirmation', response);
            $('#input_password_confirmation_id').removeClass(' is-invalid');
        }
    }
}

function validationHide(id_field, response) {
    return $(id_field).text(response.errors.name).hide();
}