var payments_backend_url = '/admin/getForAjaxWithTrashed';
var payments_datatable;
$(document).ready(function () {
    payments_datatable = $('#payments_datatable').DataTable({
        "lengthMenu" : [[10, 25, 50, -1], [10, 25, 50, "All"]],
        'processing' : true,
        'serverSide' : true,
        ajax : payments_backend_url,
        'columns' : [
            {'data' : 'id'},
            {'data' : 'client_id'},
            {'data' : 'order_id'},
            {'data' : 'status'},
            {'data' : 'order_ammount'},
            {'data' : 'stripe_response'}
        ]
    });
    
    $('#show_errors_checkbox').on('change', function () {
        if (this.checked) {
            payments_datatable.ajax.url(payments_backend_url + '?errors=1');
            payments_datatable.draw();
        } else {
            payments_datatable.ajax.url(payments_backend_url + '?errors=0');
            payments_datatable.draw();
        }
    });
});
