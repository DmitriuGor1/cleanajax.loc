$(document).ready(function () {
    $('.dropdown-item').click(function (e) {
        e.preventDefault();
        console.log($(this).val());
        console.log($(this).data('id'));
        //console.log($(e.target).val());
        $(".dropdown-menu").removeClass('show');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax ({
            dataType : 'json',
            url : "/admin/changeStatus",
            method : 'post',
            data : {
                status: $(this).val(),
                id : $(this).data('id'),
            },

        }).done( function (response) {
            console.log(response);
            $('#status-btn-' + response.result.id).text(response.result.status);
        });
    });
});