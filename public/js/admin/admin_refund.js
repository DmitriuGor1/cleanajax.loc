$(document).ready(function () {
    $( '.msg' ).hide(8400)
    $('.refund-btn').click(function (e) {
        e.preventDefault();
        if (!confirm("Are You Sure to refund???")) {
            return false;
        }

        let bt = e.target; 

        // tr which click
        let parent = $(bt).parents('tr').get(0); 

        // get id 
        let payment_id = $(bt).parents('tr').find('th:eq(0)').text(); 
        //let order_id = $(bt).parents('tr').find('th:eq(1)').text(); 

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax ({
            dataType : 'json',
            url : "/admin/makeRefund",
            method : 'post',
            data : {
                payment_id: payment_id,
            },

        }).done( function (response) {
            console.log(response);
            $(parent).hide(3000);
        });
    });
});
