function myFunction(id) {
    $(document).ready(function () {
        if (!confirm("Are You Sure to edit Price")){
            return false;
        }
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.ajax ({
            dataType : 'json',
            url : "/admin/editPrice",
            method : 'post',
            data : {
                price : $('#price-' + id).val(),
                id : id
            },
            // change part html callback function ajax
        }).done( function (response) {
            console.log(response);
            $('#static_price-' + response.result.id).text(response.result.price);

            let msg = "Price was changed";

            if (response.result.price != null) {
                alert(msg);
            }
        });
    });
}