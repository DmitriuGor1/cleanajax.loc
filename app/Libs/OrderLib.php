<?php 

namespace App\Libs;

use App\Order;
use App\Client;
use App\User;
use App\Price;
use App\Payment;
use Mpdf\Mpdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\OrderStoreRequest;
use Illuminate\Support\Facades\Storage;
use Image;
use Exception;
use App\Exceptions\OrderPhoneException;
use Propaganistas\LaravelPhone\PhoneNumber;

class OrderLib
{
    private $pricing;

    public function calculateTotalPrice(Order $order)
    {
        $total_price = (
            ($order->flat_area * $this->getServicePrice('flat_area')) +
            ($order->rooms * $this->getServicePrice('rooms')) + 
            ($order->bathroom * $this->getServicePrice('bathroom')) +
            ($order->kitchen * $this->getServicePrice('kitchen')) +
            ($order->wardrobes * $this->getServicePrice('wardrobe')) +
            ($order->animals * $this->getServicePrice('animals')) +
            ($order->children * $this->getServicePrice('children')) +
            ($order->adults * $this->getServicePrice('adults')) +
            ($order->refrigerator * $this->getServicePrice('refregirator'))
        );

        return $total_price;
    }

    public function getServicePrice(string $serviceName)
    {
        if (is_null($this->pricing)) {
            $this->fetchPrices();
        }

        return (
            !empty($this->pricing[$serviceName])
                ? $this->pricing[$serviceName]
                : 0
        );
    }

    public function fetchPrices()
    {
        $this->pricing = [];
        $pricesModels = Price::get();
        if (!empty($pricesModels)) {
            foreach ($pricesModels as $value) {
                $this->pricing[$value->title] = $value->price;
            }
        }
    }

    public static function getOrder(int $order_id)
    {
        return Order::find($order_id);
    }

    public static function getClient(int $id)
    {
        return Client::find($id);
    }

    public static function createClient(array $data)
    {
        //option for phone validation
        $valid_phone = PhoneNumber::make($data['phone'], 'UA');
        
        $client = Client::updateOrCreate(
            ['phone' => $valid_phone],
            ['email' => $data['email'],
            'surname' => $data['surname'],
            'name' => $data['name'],
        ]);

        return $client;
    }

    public static function createOrder(array $data, Client $client)
    {
        $order = Order::updateOrCreate(
            ['client_id' => $client->id],
            [
                'status' => 'created',
                'order_ammount' => 0,
                'address' => $data['address'],
                'flat_area' => $data['flat_area'],
                'rooms' => $data['rooms'],
                'bathroom' => $data['bathroom'],
                'kitchen' => $data['kitchen'],
                'refrigerator' => $data['refrigerator'],
                'wardrobes' => $data['wardrobes'],
                'animals' => $data['animals'],
                'children' => $data['children'],
                'adults' => $data['adults'],
            ]
        );
        $order = self::saveImage($data, $order);
        $order->order_ammount = (new OrderLib)->calculateTotalPrice($order);
        $order->save();

        session(['order_id' => $order->id, 'client_id' => $client->id]);
        return $order;
    }

    public static function saveImage(array $data, Order $order)
    {
        if (!empty($data['image'])) {
            $image = $data['image'];

            // create image name
            $imagename = time() . '.' . $image->extension();
            $destinationPath = storage_path('app/public/image');
            $img = Image::make($image->path());

            // resize image
            $img->resize(
                300,
                300,
                function ($constraint) {
                    $constraint->aspectRatio();
                }
            )->save($destinationPath . '/' . $imagename);
            $order->image = '/image/' . $imagename;
            $order->save();
        }
        return $order;
    }

    public static function getUsers()
    {
        return User::all();
    }

    public static function getClients()
    {
        return Client::all();
    }
}
