<?php 

namespace App\Libs;

use App\Order;
use App\Client;
use App\User;
use App\Price;
use App\Payment;
use Mpdf\Mpdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\URL;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;
use App\Http\Requests\OrderStoreRequest;
use Illuminate\Support\Facades\Storage;
use Image;
use Exception;
use App\Exceptions\OrderPhoneException;

class CalculateOrderLib
{
    public static function calculateCharge(
        $token, int $amount, Client $client
    ) {
        return self::makeStripe($token, $amount, $client);
    }

    public static function makeStripe($token, int $amount, Client $client)
    {
        $stripe = Stripe::make(env('STRIPE_API_KEY'));
        if (empty($client->stripe_customer_id)) {
            self::makeCustomer($client, $stripe);
        }

        if (! empty($token)) {
            return self::makeCharge($token, $amount, $stripe);
        }
    }

    public static function makeCustomer(Client $client, $stripe)
    {
        $customer = $stripe->customers()->create(
            [
                'email' => $client->email,
                'name' => $client->name,
            ]
        );
            $client->stripe_customer_id = $customer['id'];
            $client->save(); 

        return true;
    }

    public static function makeCharge($token, int $amount, $stripe)
    {
        try {
            $charge =  $stripe->charges()->create(
                [
                    'amount' => $amount,
                    'currency' => 'usd',
                    'source' => $token,
                ]
            );
        } catch (Exception $e) {
            $message = $e->getMessage();
            return ['status' => 'error', 'error' => $message];
        }
        return['status'=> 'ok', 'data' => $charge];
    }

    public static function paymentCreateOrderSave(
        Order $order, Client $client, $charge
    ) {
        $paymentData = [];
        $paymentData['client_id'] = $client->id;
        $paymentData['order_id'] = $order->id;
        $paymentData['order_ammount'] = $order->order_ammount;
        $paymentData['status'] = 'fulfilled';
        $paymentData['stripe_response'] = json_encode($charge);
        $payment = Payment::create($paymentData); // save Payment into db
        $order->status = 'paid';
        $order->save();
        return $order;
    }

    public static function createErrorPayment(
        array $charge, Order $order, Client $client
    ) {
        $payment = Payment::create([
                    'stripe_response' => json_encode($charge),
                    'client_id' => $client->id,
                    'order_id' => '0',
                    'order_ammount' => $order->order_ammount,
                    'status' => 'error',
                ]);
        
        return $payment;
    }

    public static function sendMailWithPdf(Order $order) 
    {
        $users = OrderLib::getUsers(); // admin's email
        foreach ($users as $user) {
            $admin_emails = $user->email;
        }

        $html = View::make('order.export_pdf', ['order' => $order])->render();
        $mpdf = new Mpdf();
        $mpdf->WriteHTML($html);
        $pdf = $mpdf->Output('', \Mpdf\Output\Destination::STRING_RETURN);
        $emails = [$order->client->email, $admin_emails];

        Mail::raw(
            "Thank you for choose our company.
            Here is Order details to your order # $order->id",
            function($message) use ($emails, $pdf)
        {
            $message->from('cleanJs@admin.com', 'cleanJs.loc');
            $message->to($emails);
            $message->attachData(
                $pdf, 'order.pdf',
                [
                    'mime' => 'application/pdf',
                ]
            );
        });

        return true;
    }
}
