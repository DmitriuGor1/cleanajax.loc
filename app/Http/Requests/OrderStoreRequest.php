<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use Propaganistas\LaravelPhone\PhoneNumber;

class OrderStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'name' => 'required|string|max:255',
            'surname' => 'required|string|max:255',
            'email' => 'required|email:rfc,dns|max:255',
            'phone' => 'phone:US,UA,RU,UK,mobile',
            'address' => 'required|max:255',
            'flat_area' => 'required|numeric|max:2000',
            'rooms' => 'between:0,50',
            'bathroom' => ['regex:/^\d+(?:\.\d)?$/','min:0','max:5'],
            'kitchen' => 'nullable|between:0,10',
            'refrigerator' => 'max:20',
            'wardrobes' => 'max:20',
            'animals' => 'max:20',
            'children' => 'max:20',
            'adults' => 'required|numeric|max:100',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];

        return $rules;
    }
}
