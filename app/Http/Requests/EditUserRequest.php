<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class EditUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required|string|max:250',
            'password' => 'required|string|min:8|confirmed',
        ];

        if (!empty($this->user_id)) {
            $rules['email'] = 'required|email:rfc,dns|string|max:250';
        } else {
            $rules['email'] = 'required|email:rfc,dns|string|max:250|unique:users,email';
        }

        return $rules;
    }
}
