<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;
use App\Price;

class StorePriceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
       return [
            
            'flat_area' => 'nullable|numeric',
            'rooms' => 'nullable|numeric',
            'bathroom' => 'nullable|numeric',
            'kitchen' => 'nullable|numeric',
            'refrigerator' => 'nullable|numeric',
            'wardrobes' => 'nullable|numeric',
            'animals' => 'nullable|numeric',
            'children' => 'nullable|numeric',
            'adults' => 'nullable|numeric',
        ];
        
    }
}
