<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Order;
use App\Payment;
use App\Client;
use App\Price;
use App\User;
use App\Http\Requests\StorePriceRequest;
use App\Http\Requests\UserPassStoreRequest;
use App\Http\Requests\EditUserRequest;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;
use App\Libs\AdminLib;
use Mpdf\Mpdf;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;

class AdminController extends Controller
{
    public function index(Request $request)
    {
        $orders = (new AdminLib)->getOrdersForMonth();
        $canceled = (new AdminLib)->getCanceledOrders();
        $canceled_amount = (new AdminLib)->getCanceledAmount($canceled);
        $canceled_quantity = (new AdminLib)->getCanceledQuantity($canceled);
        $data_payment = (new AdminLib)->getAverageOrder(
            $canceled_amount, $canceled_quantity
        );
        
        return (
            view(
                'admin.index',
                [
                    'orders' => $orders,
                    'data_payment' => $data_payment,
                    'id' => Auth::id(),
                    'canceled_amount' => $canceled_amount,
                ]
            )
        );
    }

    public function changePrice()
    {
        $prices = (new AdminLib)->getPrices();

        return view('admin.changePrice', ['prices' => $prices]);
    }

    public function editPrice(StorePriceRequest $request)
    {
        (new AdminLib)->updatePrice($request->all());
        $price = (new AdminLib)->getPrice($request->id);
        if ($price) {
            return response()->json(
                [
                    'success' => true,
                    'result' => [
                        'id' => $price->id,
                        'price' => $price->price
                    ]
                ]
            );
        }

        return response()->json([
            'success' => false,
            'result' => []
        ]);
    }

    public function paymentsErrors(Request $request)
    {
            $payments = (new AdminLib)->getErrorPaymentsWithTrashed();
            return view(
            'admin.payments_errors',
                [
                    'payments' => $payments,
                ]
            );
    }

    public function getForAjaxWithTrashed(Request $request)
    {
        //dd($request->length);
        //dd($request->start);
        $length = !empty($request->length)
            ? $request->length
            : 10;

        $search = !empty($request->search['value'])
            ? $request->search['value']
            : false;

        $counter = $request->draw;

        if ($request['errors'] == 0 || is_null($request['errors'])) {
            $query = (new AdminLib)->getErrorPayments($search, $length);
        } else {
            $query = (new AdminLib)->getErrorPaymentsWithTrashed
            (
                $search, $length
            );
        }

        $data = (new AdminLib)->generateTableData($query);
        $response = [
            'draw' => $counter++,
            'recordsTotal' => $query->count(),
            'recordsFiltered' => $query->count(),
            'data' => $data,
        ];

        return response()->json($response);
    }

    public function refund()
    {
        $payments = (new AdminLib)->getPaymentsRefundforAdmin();

        return view('admin.refund', ['payments' => $payments]);
    }

    public function makeRefund(Request $request)
    {
        $payment = (new AdminLib)->makeAdminRefund($request->all());
        $response = ['id' => $payment->id];

        return response()->json([
            'success' => 'true',
            'result' => $response,
        ]);
    }

    public function superAdmin()
    {
        return view('admin.super.super_admin', ['users' => User::all()]);
    }

    public function deleteUser(Request $request)
    {
        $delete = (new AdminLib)->superDeleteUser($request->user_id);
        if ($delete) 
            return response()->json([
                'success' => 'success',
            ]);
        return response()->json([
            'success' => 'false',
        ]);
    }

    public function editUser(Request $request)
    {
        $user = (new AdminLib)->getUser($request->user_id);
        if (empty($user)) {
            return redirect()->route(
                'admin.super'
            )->withErrors([
                "User with #" . $request->user_id . " doesn't exists"
            ]);
        }
        return view(
            'admin.super.super_edit',
            [
                'user_id' => $request->user_id,
                'user' => $user
            ]
        );
    }

    public function superAdminUser(EditUserRequest $request)
    {
        $create = (new AdminLib)->superAdminCreateUser($request->all());
        if ($create)
            return response()->json([
                'success' => 'success',
            ]);
    }

    public function storeUser(EditUserRequest $request)
    {
        $edit = (new AdminLib)->storeUser($request->all());

        if ($edit)
            return response()->json([
                'success' => 'success',
            ]);

        return response()->json([
            'success' => 'false',
        ]);
    }

    public function createUser(Request $request)
    {
        return view('admin.super.super_create');
    }

    public function refundView(Request $request)
    {   
        (new AdminLib)->refundViewPayment($request->session()->get('order_id'));

        return response()->json([
            'success' => true,
        ]);
    }

    public function orderDetails(Request $request)
    {
        $orders = (new AdminLib)->getOrderDetails($request->id);

        return view('admin.orderDetails', ['orders' => $orders]);
    }

    public function changeStatus(Request $request)
    {
        $order = (new AdminLib)->storeStatus($request->all());
        $response = [
            'id' => $order->id,
            'status' => $order->status,
        ];

        return response()->json([
            'success' => 'true',
            'result' => $response,
        ]);
    }
}
