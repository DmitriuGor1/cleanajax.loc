<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\Order;
use App\Price;
use App\User;
use App\Libs\OrderLib;
use App\Libs\CalculateOrderLib;
use Illuminate\Support\Facades\URL;
use Illuminate\Support\Facades\Session;
use Cartalyst\Stripe\Laravel\Facades\Stripe;
use App\Payment;
use App\Http\Requests\OrderStoreRequest;
use Illuminate\Support\Facades\View;
use Mpdf\Mpdf;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Exception;
use App\Exceptions\OrderPhoneException;

class OrderController extends Controller
{
    public function index()
    {
        return view('order.index');
    }

    public function images()
    {
        return view('order.images');
    }

    public function store(OrderStoreRequest $request)
    {
        $client = OrderLib::createClient($request->all());
        $order = OrderLib::createOrder($request->all(), $client);

        return response()->json([
            'success' => true,
            'result' => [
                'address' => $order->address,
                'flat_area' => $order->flat_area,
                'rooms' => $order->rooms,
                'bathroom' => $order->bathroom,
                'kitchen' => $order->kitchen,
                'refrigerator' => $order->refrigerator,
                'wardrobes' => $order->wardrobes,
                'animals' => $order->animals,
                'children' => $order->children,
                'adults' => $order->adults,
                'order_ammount' => $order->order_ammount,
                'image' => $order->image,
            ]
        ]);
    }

    public function pay(Request $request)
    {
        $order = OrderLib::getOrder(session()->get('order_id'));
        $client = OrderLib::getClient(session()->get('client_id'));
        $stripeToken = $request->stripeToken;

        //stripe charge
        $charge = CalculateOrderLib::calculateCharge(
            $stripeToken, $order->order_ammount, $client
        );

        if ($charge['status'] === 'ok') {
            $orderSave = CalculateOrderLib::paymentCreateOrderSave(
                $order,
                $client,
                $charge
            );
        } else {
            $error_payment = CalculateOrderLib::createErrorPayment(
                $charge, $order, $client
            );

            return response()->json([
                'error' => false,
                'result' => [
                    'message' => $error_payment->stripe_response,
                ]
            ]);
        }
        //Send Mail with PDF to client and admin
        CalculateOrderLib::sendMailWithPdf($order);

        return response()->json([
            'success' => true,
            'result' => [
                'message' => 'Pay complete successfully',
            ]
        ]);
    }
}
