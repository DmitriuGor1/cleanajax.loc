<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subscriptions extends Model
{
    protected $fillable = [
        'id', 
        'client_id', 
        'name', 
        'stripe_id', 
        'stripe_status', 
        'stripe_plan',
        'quantity',
        'card_brand',
        'trial_ends_at',
        'ends_at'
    ];
}
