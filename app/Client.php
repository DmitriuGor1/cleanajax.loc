<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;

class Client extends Model
{
    use Billable;

    protected $fillable = [
        'name', 
        'surname', 
        'email', 
        'phone', 
        'stripe_customer_id', 
        'add_info',
        'stripe_id',
        'card_brand',
        'card_last_four',
        'trial_ends_at'
    ];
}
