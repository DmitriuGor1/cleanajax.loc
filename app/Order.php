<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Http\Requests\OrderStoreRequest;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'client_id',
        'status',
        'order_ammount',
        'address',
        'flat_area',
        'rooms',
        'bathroom',
        'kitchen',
        'refrigerator',
        'wardrobes',
        'animals',
        'children',
        'adults',
        'image',
        'deleted_at'
    ];

    protected $with = ['client'];

    public function client()
    {
        return $this->belongsTo('App\Client', 'client_id', 'id');
    }
}
