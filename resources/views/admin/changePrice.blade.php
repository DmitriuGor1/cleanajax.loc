@extends('layouts.app')

@section('content')

@if(session()->has('message'))
    <div class="alert bg-dark text-white text-center">
        {{ session()->get('message') }}
    </div>
@endif
                
<div class="inner cover mt-5">
    <h1 class="text-center">Price</h1>
        <div class="container">
            <table class="table" id="datatable">
                <thead class="table-dark table-bordered text-center">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Title</th>
                        <th scope="col">Price</th>
                        <th scope="col">Input</th>
                        <th scope="col">Edit</th>
                    </tr>
                </thead>
                <tbody class="table-bordered text-center">
                    @foreach ($prices as $price)
                            <tr>
                                <td>{{ $price->id }}</td>
                                <td data-name="{{ $price->title }}">
                                    {{ $price->title }}
                                </td>
                                <td
                                    id="static_price-{{ $price->id }}"
                                    class="static_price"
                                >
                                    {{ $price->price }}
                                </td>
                                <td>
                                    <div class="input_changePrice text-center">
                                        <input
                                            type="text"
                                            name="price"
                                            value="{{ $price->price }}"
                                            class="input_editPrice {{ $errors->has('price') ? 'is-invalid' : '' }}"
                                            id="price-{{ $price->id }}"
                                        >
                                         @if ($errors->has('price'))
                                            <div class="invalid-feedback">
                                                <strong>
                                                    {{ $errors->first('price') }}
                                                </strong>
                                            </div>
                                        @endif
                                    </div>
                                </td>
                                <td>
                                    <button
                                        type="submit"
                                        class="btn btn-dark"
                                        onclick="return myFunction( {{ $price->id }} );"
                                    >
                                        Edit
                                    </button>
                                </td>
                            </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
</div>

<script src="{{ asset('js/admin/changePrice.js') }}"></script>
@endsection