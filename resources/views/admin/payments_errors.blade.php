@extends('layouts.app')

@section('content')

<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.css">

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

    <div class="d-flex justify-content-center mt-5">
        <div class="col-9">
            <h4 class="text-center">
                <strong>Payment Errors</strong>
            </h4>
            <div class="custom-checkbox">
                <p class="text-right">
                    <input
                        type="checkbox"
                        class="custom-control-input"
                        id="show_errors_checkbox"
                    >
                    <label
                        class="custom-control-label"
                        for="show_errors_checkbox"
                    >
                        Payments with trashed
                    </label>
                </p>
            </div>
            <table class="table table-bordered" id="payments_datatable">
                <thead class="table-dark">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">ClientId</th>
                        <th scope="col">OrderId</th>
                        <th scope="col">Status</th>
                        <th scope="col">Amount</th>
                        <th class="big-col">Stripe Response</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

<script src="{{ asset('js/admin/payment_errors.js') }}"></script>
@endsection