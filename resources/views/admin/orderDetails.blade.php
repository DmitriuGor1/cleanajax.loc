@extends('layouts.app')

@section('content')

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
    
    <h2 class="text-center mt-5">Order details</h2>
    <div class="container mt-5">
        <table class="table table-bordered text-center">
            @if (count($orders) > 0)
            @foreach ($orders as $order)
                <thead class="table-dark">
                    <tr>
                        <th scope="col">OrderId</th>
                        <th scope="col">ClientId</th>
                        <th scope="col">Name</th>
                        <th scope="col">Surname</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Email</th>
                        <th scope="col">Address</th>
                        <th scope="col">Status</th>
                        <th scope="col">Amount</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="col">{{ $order->id }}</th>
                        <th scope="col">{{ $order->client->id }}</th>
                        <th scope="col">{{ $order->client->name }}</th>
                        <th scope="col">{{ $order->client->surname }}</th>
                        <th scope="col">{{ $order->client->phone }}</th>
                        <th scope="col">{{ $order->client->email }}</th>
                        <th scope="col">{{ $order->address }}</th>
                        <th scope="col">{{ $order->status }}</th>
                        <th scope="col">{{ $order->order_ammount }}</th>
                    </tr>
                 </tbody>
            @endforeach
            @endif
        </table>
        
        <table class="table table-bordered text-center">
            @if (count($orders) > 0)
            @foreach ($orders as $order) 
                <thead class="table-dark">
                    <tr>
                        <th scope="col">Flat area</th>
                        <th scope="col">Rooms</th>
                        <th scope="col">Bathroom</th>
                        <th scope="col">Kitchen</th>
                        <th scope="col">Refrigerator</th>
                        <th scope="col">Wardrobes</th>
                        <th scope="col">Animals</th>
                        <th scope="col">Children</th>
                        <th scope="col">Adults</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th scope="col">
                            {{ $order->flat_area }}
                        </th>
                        <th scope="col">
                            {{ $order->rooms ? $order->rooms : '0' }}
                        </th>
                        <th scope="col">
                            {{ $order->bathroom ? $order->bathroom : '0'}}
                        </th>
                        <th scope="col">
                            {{ $order->kitchen ? $order->kitchen : '0' }}
                        </th>
                        <th scope="col">
                            {{ $order->refrigerator ? $order->refrigerator : '0' }}
                        </th>
                        <th scope="col">
                            {{ $order->wardrobes ? $order->wardrobes : '0' }}
                        </th>
                        <th scope="col">
                            {{ $order->animals ? $order->animals : '0' }}
                        </th>
                        <th scope="col">
                            {{ $order->children ? $order->children : '0' }}
                        </th>
                        <th scope="col">
                            {{ $order->adults ? $order->adults : '0' }}
                        </th>
                    </tr>
                </tbody>
        </table>
            <div class="container">
                <div class="row">
                    <div class="col text-center">
                        @if(!empty($order->image))
                        <img
                            src="{{ asset('/storage/'. $order->image) }}"
                            alt="flat"
                        >
                        @endif
                    </div>
                </div>
            </div>
            @endforeach
            @endif
    </div>
@endsection