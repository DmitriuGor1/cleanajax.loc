@extends('layouts.app')

@section('content')

@if(session()->has('message'))
    <div class="alert bg-dark text-white text-center msg">
        {{ session()->get('message') }}
    </div>
@endif
    
<div class="inner cover mt-5">
    <h2 class="text-center">Orders Information</h2>
        <div class="container mt-5">
            <table class="table table-bordered text-center">
                <thead class="table-dark table-bordered">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Order total amount per month</th>
                        <th scope="col">
                            Order total canceled amount per month
                        </th>
                        <th scope="col">Order quantity per month</th>
                        <th scope="col">Order avarage cost</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <th scope="row">Items</th>
                    <td>{{ $data_payment['total_price'] }} USD</td>
                    <td>{{ $canceled_amount }} USD</td>
                    <td>{{ $data_payment['total_quantity'] }}</td>
                    <td>{{ $data_payment['order_avarage'] }} USD</td>
                    </tr>
                </tbody>
            </table>

            <table class="table table-bordered text-center">
                <thead class="table-dark table-bordered">
                    <tr>
                        <th scope="col">ClientId</th>
                        <th scope="col">Name</th>
                        <th scope="col">Phone</th>
                        <th scope="col">Amount</th>
                        <th scope="col">Sq.m</th>
                        <th scope="col">Status</th>
                        <th scope="col">Order details</th>
                    </tr>
                </thead>
                <tbody>
                @if (count($orders) > 0)
                @foreach ($orders as $order) 
                        <tr>
                            <th scope="row">{{ $order->client->id }}</th>
                            <td>{{ $order->client->name }}</td>
                            <td>{{ $order->client->phone }}</td>
                            <td> {{ $order->order_ammount }}</td>
                            <td> {{ $order->flat_area }}</td>
                            <td>
                                <form>
                                @csrf
                                    <div class="btn-group">
                                        <button 
                                            type="button"
                                            class="btn btn-dark
                                                dropdown-toggle
                                                button_admin_index"
                                            id="status-btn-{{ $order->id }}"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"
                                        >
                                            {{ $order->status }}
                                        </button>
                                        <div class="dropdown-menu">
                                           <input
                                                type="submit"
                                                name="status"
                                                value="completed"
                                                class="dropdown-item"
                                                data-id="{{ $order->id }}"
                                           >
                                           <input
                                                type="submit"
                                                name="status"
                                                value="canceled"
                                                class="dropdown-item"
                                                data-id="{{ $order->id }}"
                                           >
                                           <input
                                                type="submit"
                                                name="status"
                                                value="paid"
                                                data-id="{{ $order->id }}"
                                                class="dropdown-item"
                                           >
                                        </div>
                                    </div>
                                </form>
                            </td>
                            <td>
                                <a
                                    href="{{ route('admin.orderDetails', ['id' => $order->id]) }}"
                                    class="text-dark"
                                >
                                    <strong>Order details</strong>
                                </a>
                            </td>
                        </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>

<script src="{{ asset('js/admin/changeStatus.js') }}"></script>
@endsection