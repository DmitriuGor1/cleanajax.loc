@extends('layouts.app')

@section('content')

@if(session()->has('message'))
    <div class="alert alert-success text-center text-danger msg">
        <strong>{{ session()->get('message') }}</strong>
    </div>
@endif

    <div class="container mt-5">
        <table class="table table-bordered text-center mt-5">
            <thead class="table-dark">
                <tr>
                    <th scope="col">Payment_Id</th>
                    <th scope="col">Order_Id</th>
                    <th scope="col">Client_Id</th>
                    <th scope="col">ClientName</th>
                    <th scope="col">Address</th>
                    <th scope="col">Status</th>
                    <th scope="col">Amount</th>
                    <th scope="col">Refunds</th>
                </tr>
                @if(count($payments) > 0)
                @foreach ($payments as $payment)
                </thead>
                <tbody>
                    <tr>
                        <th>{{ $payment->id }}</th>
                        <th>{{ $payment->order->id }}</th>
                        <th>{{ $payment->client_id }}</th>
                        <th>{{ $payment->order->client->name }}</th>
                        <th>{{ $payment->order->address }}</th>
                        <th>{{ $payment->order->status }}</th>
                        <th>{{ $payment->order->order_ammount }}</th>
                        <th>
                            <form>
                            @csrf
                                <div class="btn-group">
                                    <button
                                        type="submit"
                                        class="btn btn-danger refund-btn"
                                    >
                                        Refund
                                    </button>
                                </div>
                            </form>
                        </th>
                    </tr>
                @endforeach
                @else
                    <h3 class="text-dark text-center">NoOne order to refund</h3>
                 @endif
            </tbody>
        </table>
    </div>

<script src="{{ asset('js/admin/admin_refund.js') }}"></script>
@endsection