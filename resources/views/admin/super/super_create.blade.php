@extends('layouts.app')

@section('content')

@if(session()->has('message'))
        <div class="alert bg-dark text-center text-white">
            {{ session()->get('message') }}
        </div>
@endif

    <div class="inner cover mt-5">
    <h2 class="text-center">Create Admin</h2>
        <div class="table-super_create">
            <form action="" method="POST">
                {{ csrf_field() }}
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text bg-dark text-white">
                            &ensp;&ensp;Name&ensp;&ensp;
                        </div>
                    </div>
                    <input
                        type="text"
                        name="name"
                        class="form-control"
                        id="input_name_id"
                        value="{{ old('name') }}"
                    >

                        <div class="invalid-feedback" id="error_name">
                            <strong></strong>
                        </div>
        
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text bg-dark text-white">
                            &ensp;&ensp;Email&ensp;&ensp;
                        </div>
                    </div>
                    <input
                        type="email"
                        name="email"
                        class="form-control"
                        id="input_email_id"
                        value="{{ old('email') }}"
                    >
                        <div class="invalid-feedback" id="error_email">
                            <strong></strong>
                        </div>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text bg-dark text-white">
                            Password
                        </div>
                    </div>
                    <input
                        type="password"
                        name="password"
                        class="form-control"
                        id="input_password_id"
                    >
                        <div class="invalid-feedback" id="error_password">
                            <strong></strong>
                        </div>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text bg-dark text-white">
                            &ensp;Confirm&ensp;
                        </div>
                    </div>
                    <input
                        type="password"
                        name="password_confirmation"
                        class="form-control"
                        id="input_password_confirmation_id"
                    >
                        <div class="invalid-feedback"
                                id="error_password_confirmation">
                                <strong></strong>
                        </div>
                </div>
                <input
                    type="submit"
                    value="Create"
                    class="btn btn-dark btn-block btn-flat btn-submit"
                >
            </form>
        </div>
    </div>
    
<script src="{{ asset('js/admin/super/super_admin_create.js') }}"></script>
@endsection