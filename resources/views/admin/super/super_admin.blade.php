@extends('layouts.app')

@section('content')

@if(session()->has('message'))
    <div class="alert bg-dark text-center text-white msg">
        {{ session()->get('message') }}
    </div>
@endif
    <h2 class="text-center mt-5">Super Admin Page</h2>
    <div class="d-flex justify-content-center mt-5">
        <div class="container">
            <table class="table table-bordered">
                <thead class="table-dark text-center">
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Name</th>
                        <th scope="col">Email</th>
                        <th scope="col">Password</th>
                        <th scope="col">
                            <a class="btn btn-danger" 
                                href=" {{ route('createUser') }}">
                                Create
                            </a>
                        </th>
                        <th scope="col">Edit</th>
                        <th scope="col">Delete</th>
                    </tr>
                </thead>
                <tbody class="text-center">
                    @foreach ($users as $user)
                    <tr>
                        <td>{{ $user->id }} </td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }} </td>
                        <td>{{ $user->password }} </td>
                        <td>Created</td>
                        <td>
                            <a 
                                class="btn btn-dark" 
                                href="{{ route(
                                            'editUser', 
                                            ['user_id' => $user->id] 
                                        ) }}"
                            >
                                    &ensp;Edit&ensp;
                            </a>
                        </td>
                        <td>
                            <a 
                                class="btn btn-dark"
                                id="btn-del-{{ $user->id }}"
                                onclick="return myFunction( {{ $user->id }} ); 
                                "href="{{ route(
                                            'deleteUser', 
                                            ['user_id' => $user->id] 
                                        ) }}"
                            >
                                Delete
                            </a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

<script src="{{ asset('js/admin/super/super_admin_delete.js') }}"></script>
@endsection