<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        
        <title>CleaningJS</title>

        <script src="{{ asset('js/app.js') }}"></script>
        <script src="{{ asset('js/lightgallery/jquery.js') }}"></script>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" 
            rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
       
        <!-- Style -->
        <link href="{{ asset('css/app.css') }}" rel="stylesheet">
        <link href="{{ asset('css/album.css') }}" rel="stylesheet">
        <link href="{{ asset('css/form.css') }}" rel="stylesheet">
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet">

    </head>
    <body>
        <div class="wrapper">
            <div class="content">
            <header>
                <div class="collapse bg-dark" id="navbarHeader">
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-9 col-md-7 py-4">
                                <h4 class="text-white">
                                    <a
                                        class="text-white" 
                                        href="{{ route('order') }}"
                                    >
                                        CleaningJS
                                    </a>
                                </h4>
                                <p class="text-muted">
                                    We are an eco-friendly company, committed to protecting the environment with sustainable cleaning materials. We are one of the only cleaning companies to use only environmentally responsible or sustainable cleaning products. In addition, we use a thorough Detail-Clean Rotation System to ensure that your home looks good all the time.
                                </p>
                            </div>
                            <div class="col-sm-3 offset-md-1 py-4">
                                <h4 class="text-white">Contact</h4>
                                <ul class="list-unstyled">
                                    <li>
                                        <a
                                            href="#"
                                            class="text-white"
                                        >
                                            Follow on Twitter
                                        </a>
                                    </li>
                                    <li>
                                        <a
                                            href="#"
                                            class="text-white"
                                        >
                                            Like on Facebook
                                        </a>
                                    </li>
                                    <li>
                                        <a
                                            href="#"
                                            class="text-white"
                                            >Email us
                                        </a>
                                    </li>
                                </ul>
                                @if((Gate::allows('admin'))
                                || (Gate::allows('super_admin')))
                                <h4 class="text-white">
                                    <a
                                        class="text-white"
                                        href="{{ route('admin') }}"
                                    >
                                        For Admin
                                    </a>
                                </h4>
                                        <ul class="list-unstyled">
                                        <li>
                                            <a
                                                href="{{ route ('admin.refund') }}"
                                                class="text-white"
                                            >
                                                Refunds
                                            </a>
                                        </li>
                                        <li>
                                            <a
                                                href="{{ route ('admin.change_price') }}"
                                                class="text-white"
                                            >
                                                Edit Price
                                            </a>
                                        </li>
                                        @if((Gate::allows('super_admin')))
                                            <li>
                                                <a
                                                    href="{{ route ('admin.super') }}"
                                                    class="text-white"
                                                 >
                                             Super</a>
                                            </li>
                                        @endif
                                        <li>
                                            <a
                                                href="{{ route ('admin.paymentsErrors') }}"
                                                class="text-white"
                                            >
                                                Payment Errors
                                            </a>
                                        </li>
                                    </ul>
                                @endif
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="navbar navbar-dark bg-dark shadow-sm">
                    <div class="container d-flex justify-content-between">
                        <a 
                            href="{{  route('order.images') }}"
                            class="navbar-brand d-flex align-items-center">
                            <svg
                                xmlns="http://www.w3.org/2000/svg"
                                width="20"
                                height="20"
                                fill="none"
                                stroke="currentColor"
                                stroke-linecap="round"
                                stroke-linejoin="round"
                                stroke-width="2"
                                aria-hidden="true"
                                class="mr-2" viewBox="0 0 24 24"
                                focusable="false"
                            >
                                    <path d="M23 19a2 2 0 0 1-2 2H3a2 2 0 0 1-2-2V8a2 2 0 0 1 2-2h4l2-3h6l2 3h4a2 2 0 0 1 2 2z"/>
                                    <circle cx="12" cy="13" r="4"/>
                            </svg>
                            <strong>Album</strong>
                        </a>
                        <button
                            class="navbar-toggler"
                            type="button"
                            data-toggle="collapse"
                            data-target="#navbarHeader"
                            aria-controls="navbarHeader"
                            aria-expanded="false"
                            aria-label="Toggle navigation"
                        >
                            <span class="navbar-toggler-icon"></span>
                        </button>
                    </div>
                </div>
            </header>

            <main class="py-4">
                @yield('content')
            </main>

            </div>
        </div>
        <div class="text-muted footer">
            <div class="container">
                <p class="text-center">
                    Copyright © The Cleaning Authority, LLC All rights reserved.
                </p>
                <p>Available at participating franchised locations only. Contact your local franchised office to determine what types of products and services, @guest
                        <a
                            class="text-dark {{ (request()->is('login')) ? 'active' : '' }}"
                            href="{{ route('login') }}"
                        >
                            <strong>{{ __('Login') }}</strong>
                        </a>
                        @if (Route::has('register'))
                            <a
                                class="nav-link {{ (request()->is('register')) ? 'active' : '' }}"
                                href="{{ route('register') }}"
                            >
                                <strong>{{ __('Register') }}</strong>
                            </a>
                        @endif
                        @else 
                            <a 
                                class="text-dark"
                                href="{{ route('logout') }}"
                                onclick="
                                    event.preventDefault();
                                    document.getElementById('logout-form').submit();
                                "
                            >
                                <strong>{{ __('Logout') }}<strong>
                            </a>

                            <form
                                id="logout-form"
                                action="{{ route('logout') }}"
                                method="POST"
                                style="display: none;"
                            >
                            @csrf
                            </form>
                    @endguest are offered in your area.
                </p>
            </div>
        </div>
    </body>

<!-- <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script> -->
<script 
    src="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.3.2/js/lightgallery.js">
</script>
<script type="text/javascript" charset="utf8"
        src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.js">
</script>
</html>