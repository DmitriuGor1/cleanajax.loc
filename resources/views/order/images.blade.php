@extends('layouts.app')

@section('content')

<link rel="stylesheet" 
    href="https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.3.2/css/lightgallery.css"
/>
    <h2 class="text-center text-dark">
        <strong>Before - after</strong>
    </h2>
    <br>
    <div class="container">
        <div id="lightgallery">
            <a href="{{ asset('img/dirty1.jpeg') }}">
                <img src="{{ asset('img/thumb1.jpeg') }}"
                    width="275"
                    height="275"
                />
            </a>
            <a href="{{ asset('img/dirty1.jpeg') }}">
                <img src="{{ asset('img/thumb1.jpeg') }}"
                    width="275"
                    height="275"
                />
            </a>
            <a href="{{ asset('img/dirty1.jpeg') }}">
                <img src="{{ asset('img/thumb1.jpeg') }}"
                    width="275"
                    height="275"
                />
            </a>
            <a href="{{ asset('img/clean.jpg') }}">
                <img src="{{ asset('img/thumb2.jpg') }}"
                    width="275"
                    height="275"
                />
            </a>
            <a href="{{ asset('img/clean3.jpg') }}">
                <img src="{{ asset('img/thumb3.jpg') }}"
                    width="275"
                    height="275"
                />
            </a>
            <a href="{{ asset('img/clean4.jpg') }}">
                <img src="{{ asset('img/thumb4.jpg') }}"
                    width="275"
                    height="275"
                />
            </a>
        </div>
    </div>

<script src="{{ asset('js/lightgallery/jquery.js') }}"></script>
<script src="{{ asset('js/lightgallery/lightgallery.js') }}"></script>
<script src="{{ asset('js/lightgallery/lg-thumbnail.js') }}"></script>
<script src="{{ asset('js/lightgallery/lg-autoplay.js') }}"></script>
<script src="{{ asset('js/lightgallery/lg-fullscreen.min.js') }}"></script>
<script>
    $(document).ready(function() {
        $("#lightgallery").lightGallery({

        }); 
    });
</script>
@endsection