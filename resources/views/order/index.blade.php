@extends('layouts.app')

@section('content')

 <link href="{{ asset('css/stripe.css') }}" rel="stylesheet">
<script src="https://js.stripe.com/v3/"></script>

@if(session()->has('message'))
    <div class="alert bg-dark text-center text-white msg">
        {{ session()->get('message') }}
    </div>
@endif

    <div class="container">
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="{{ asset('img/img.svg') }}" alt="" width="72" height="72">
            <h2>We will clean your house and office</h2>
                <p class="lead">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quisquam, iste cumque amet numquam, molestias impedit est maiores laboriosam fugit quo maxime iusto natus labore error repellat officia rerum esse reiciendis?.
                </p>
        </div>

        <div class="row">
            <div class="col-md-4 order-md-2 mb-4">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-muted">Your order</span>
                </h4>
            <ul class="list-group mb-3">
                 <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">Address</h6>
                    </div>
                    <span class="text-muted" id="order_address"></span>
                 </li>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                    <h6 class="my-0">Flat_area</h6>
                    </div>
                    <span class="text-muted" id="order_flat_area"></span>
                </li>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">Rooms</h6>
                     </div>
                    <span class="text-muted" id="order_rooms"></span>
                </li>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">Bathroom</h6>
                     </div>
                    <span class="text-muted" id="order_bathroom"></span>
                </li>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">Kitchen</h6>
                     </div>
                    <span class="text-muted" id="order_kitchen"></span>
                </li>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">Refrigerator</h6>
                     </div>
                    <span class="text-muted" id="order_refrigerator"></span>
                </li>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">Wardrobes</h6>
                     </div>
                    <span class="text-muted" id="order_wardrobes"></span>
                </li>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">Animals</h6>
                     </div>
                    <span class="text-muted" id="order_animals"></span>
                </li>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">Children</h6>
                     </div>
                    <span class="text-muted" id="order_children"></span>
                </li>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">Adults</h6>
                     </div>
                    <span class="text-muted" id="order_adults"></span>
                </li>
                <li class="list-group-item d-flex justify-content-between lh-condensed">
                    <div>
                        <h6 class="my-0">Image</h6>
                     </div>
                    <span class="" id="order_image"></span>
                </li>
                <li class="list-group-item d-flex justify-content-between">
                    <span >Total (USD)</span>
                    <strong id="order_ammount"></strong>
                </li>
            </ul>
            <br>

            <div class="text-center pay-methods" style="display:none">
                <h4 class="text-danger text-center">
                    <strong>Methods for pay:</strong>
                </h4>
                <img
                    class="pay_icon"
                    src="{{ asset('img/stripe.png') }}"
                >
                <img
                    class="pay_icon_privat" 
                    src="{{ asset('img/privat24.png') }}"
                >
            </div>
        </div>

        <div class="col-md-8 order-md-1">
            <h4 class="mb-3">Customer details</h4>
            <form 
                class="needs-validation"
                method="POST"
                enctype="multipart/form-data"
                id="form_submit"
            >
            @csrf
                <div class="row">
                <div class="col-md-6 mb-3">
                    <label for="inputName">Name</label>
                    <input 
                        type="text"
                        name="name"
                        value="{{ old('name') }}"
                        class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}"
                        id="inputName"
                    >
                        <div class="invalid-feedback" id="error_inputName">
                            <strong></strong>
                        </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="inputSurname">Last name</label>
                    <input 
                        type="text"
                        name ="surname"
                        value="{{ old('surname') }}"
                        class="form-control {{ $errors->has('surname') ? 'is-invalid' : '' }}"
                        id="inputSurname"
                    >
                        <div 
                            class="invalid-feedback"
                            id="error_inputSurname"
                        >
                            <strong></strong>
                        </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="inputPhone">Phone</label>
                    <input 
                        type="text"
                        name ="phone"
                        value="{{ old('phone') }}"
                        class="form-control {{ $errors->has('phone') ? 'is-invalid' : '' }}"
                        id="inputPhone"
                    >
                        <div class="invalid-feedback" id="error_inputPhone">
                            <strong></strong>
                        </div>
                </div>
                <div class="col-md-6 mb-3">
                    <label for="inputEmail">Email</label>
                    <input 
                        type="email"
                        name="email"
                        value="{{ old('email') }}"
                        class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}"
                        id="inputEmail"
                    >
                        <div class="invalid-feedback" id="error_inputEmail">
                            <strong></strong>
                        </div>
                </div>
                </div>

        <hr class="mb-4">
        <h4 class="mb-3">Order details</h4>

        <div class="row">
            <div class="col-md-6 mb-3">
                <label for="inputAddress">Address</label>
                <input 
                    type="text"
                    name="address"
                    value="{{ old('address') }}"
                    class="form-control {{ $errors->has('address') ? 'is-invalid' : '' }} "
                    id="inputAddress"
                    placeholder="1234 Main St"
                >
                    <div class="invalid-feedback" id="error_inputAddress">
                        <strong></strong>
                    </div>
            </div> 
            <div class="col-md-6 mb-3">
                <label for="inputFlatArea">Flat Area(sq.m) </label>
                <input 
                    type="text"
                    name="flat_area"
                    value="{{ old('flat_area') }}"
                    class="form-control {{ $errors->has('flat_area') ? 'is-invalid' : '' }} "
                    id="inputFlatArea"
                >
                    <div class="invalid-feedback" id="error_inputFlatArea">
                        <strong></strong>
                    </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 mb-3">
                    <label for="inputRooms">Rooms</label>
                    <select id="inputRooms"
                        name="rooms"
                        class="custom-select d-block w-100
                            {{ $errors->has('rooms') ? 'is-invalid' : '' }}">
                        @foreach (range(0, 10) as $roomsCount)
                            <option value="{{ $roomsCount }}"
                            @if ($roomsCount == old('rooms')) selected @endif>
                                {{ $roomsCount }}
                            </option>
                        @endforeach
                    </select>
                        @if ($errors->has('rooms'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('rooms') }}</strong>
                            </div>
                        @endif
            </div>
            <div class="col-md-3 mb-3">
                <label for="inputBathroom">Bathroom</label>
                <select id="inputBathroom"
                    name="bathroom"
                    class="custom-select d-block w-100
                        {{ $errors->has('bathroom') ? 'is-invalid' : '' }}"
                >
                    @foreach (range(0, 5, 0.5) as $bathroomCount)
                        <option value="{{ $bathroomCount }}" 
                        @if ($bathroomCount == old('bathroom')) selected @endif>
                            {{ $bathroomCount }}
                        </option>
                    @endforeach
                </select>
                    @if ($errors->has('bathroom'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('bathroom') }}</strong>
                        </div>
                    @endif
            </div>
            <div class="col-md-3 mb-3">
                <label for="inputKitchen">Kitchen</label>
                <select 
                id="inputKitchen"
                name="kitchen"
                class="custom-select d-block w-100 
                    {{ $errors->has('kitchen') ? 'is-invalid' : '' }}"
                >
                    @foreach (range(0, 10) as $kitchensCount)
                        <option value="{{ $kitchensCount }}" 
                        @if ($kitchensCount == old('kitchen')) selected @endif>
                            {{ $kitchensCount }}
                        </option>
                    @endforeach
                </select>
                    @if ($errors->has('kitchen'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('kitchen') }}</strong>
                        </div>
                    @endif
            </div>
            <div class="col-md-3 mb-3">
                <label for="inputRefrigerator">Refrigerator</label>
                <select id="inputRefrigerator" name="refrigerator" 
                        class="custom-select d-block w-100">
                    @foreach (range(0, 10) as $refrigeratorCount)
                        <option 
                            value="{{ $refrigeratorCount }}" 
                            @if ($refrigeratorCount == old('refrigerator')) selected
                            @endif
                        >
                            {{ $refrigeratorCount }}
                        </option>
                    @endforeach
                </select>
                    @if ($errors->has('refrigerator'))
                        <div class="invalid-feedback">
                            <strong>
                                {{ $errors->first('refrigerator') }}
                            </strong>
                        </div>
                    @endif
            </div>
        </div>

        <div class="row">
            <div class="col-md-3 mb-3">
                <label for="inputWardrobes">Wardrobes</label>
                <select 
                    id="inputWardrobes"
                    name="wardrobes"
                    class="custom-select d-block w-100"
                >
                    @foreach (range(0, 10) as $wardrobesCount)
                        <option 
                            value="{{ $wardrobesCount }}" 
                            @if ($wardrobesCount == old('wardrobes')) selected
                            @endif
                        >
                            {{ $wardrobesCount }}
                        </option>
                    @endforeach
                </select>
                    @if ($errors->has('wardrobes'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('wardrobes') }}</strong>
                        </div>
                    @endif
            </div>
            <div class="col-md-3 mb-3">
                <label for="inputAnimals">Animals</label>
                <select 
                    id="inputAnimals"
                    name="animals"
                    class="custom-select d-block w-100"
                >
                    @foreach (range(0, 10) as $animalsCount)
                        <option 
                            value="{{ $animalsCount }}"
                            @if ($animalsCount == old('animals')) selected 
                            @endif
                        >
                            {{ $animalsCount }}
                        </option>
                    @endforeach
                </select>
                    @if ($errors->has('animals'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('animals') }}</strong>
                        </div>
                    @endif
            </div>
            <div class="col-md-3 mb-3">
                <label for="inputChildren">Children</label>
                <select
                    id="inputChildren"
                    name="children"
                    class="custom-select d-block w-100"
                >
                    @foreach (range(0, 10) as $childrenCount)
                        <option
                            value="{{ $childrenCount }}"
                            @if ($childrenCount == old('children')) selected
                            @endif
                        >
                            {{ $childrenCount }}
                        </option>
                    @endforeach
                </select>
                    @if ($errors->has('children'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('children') }}</strong>
                        </div>
                    @endif
            </div>
            <div class="col-md-3 mb-3">
                <label for="inputAdults">Adults</label>
                <select
                    id="inputAdults"
                    name="adults"
                    class="custom-select d-block w-100"
                >
                    @foreach (range(0, 10) as $adultsCount)
                        <option 
                            value="{{ $adultsCount }}"
                            @if ($adultsCount == old('adults')) selected
                            @endif
                        >
                            {{ $adultsCount }}
                        </option>
                    @endforeach
                </select>
                    @if ($errors->has('adults'))
                        <div class="invalid-feedback">
                            <strong>{{ $errors->first('adults') }}</strong>
                        </div>
                    @endif
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mb-3">
                <label for="inputImage">Image</label>
                <input
                    type="file"
                    name="image"
                    id="inputImage"
                >
            </div>
        </div>

         <hr class="mb-4">
        <button
            class="btn btn-dark btn-lg btn-block btn-submit"
            type="submit"
        >
                Calculate
        </button>
    </form>
    </div>
</div>
<div class="container">
    <div class="row">
        <div class="col-md-8">
             <div class="stripe-response" style="display: none;">
                <div>
                    <h2 class="text-success text-center">
                        <strong>Successfully pay complete</strong>
                    </h2>
                    <br>
                        <p class="text-success text-center">
                            <strong>
                                We send order details on your email
                            </strong>  
                        </p>
                    <hr>
                    <br>
                </div>
                <div>
                    <h5 class="text-danger"><strong>Cancele order</strong></h5>
                    <br>
                    <a class="btn btn-danger refund" 
                        href="">
                        Refund
                    </a>
                </div>
            </div>
            <div class="row stripe-response-fail" style="display: none;">
                <div>
                    <h2 class="text-danger text-center">
                        <strong>Error</strong>
                    </h2>
                        <p class="text-danger text-center" id="stripe-fail">
                        </p>
                        <br>
                        <p class="text-danger text-center">
                            enter valid card number
                        </p>
                    <hr>
                </div>
            </div>
        </div>
        <div class="stripe-pay col-md-4" style="display: none;">
            <h4 class="text-danger text-center">
                <strong>Stripe</strong>
            </h4>
                <!-- Stripe pay   -->
            <form 
                action=" {{ route('order.pay') }} " 
                method="post" 
                id="payment-form"
            >
                @csrf
                <div class="">
                    <label for="card-element" class="text-center">
                         <span class="text-success">Credit or debit card</span>
                    </label>
                    <div id="card-element">
                    <!-- a Stripe Element will be inserted here. -->
                    </div>

                <!-- Used to display form errors -->
                    <div id="card-errors" role="alert">
                        @if(session()->has('message'))
                            <div class="alert alert-danger">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                    </div>
                </div>
                <div class="stripe-button" >
                    <button 
                        class="btn-danger pull-right">
                            Submit Payment
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>

<script src="{{ asset('js/order/submitForm.js') }}"></script>
<script src="{{ asset('js/stripe/processing_stripe_response.js') }}"></script>
<script src="{{ asset('js/stripe/charge.js') }}"></script>
<script src="{{ asset('js/order/refund.js') }}"></script>
@endsection