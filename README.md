CleanJs.loc

Description:
- cleaning company on single-page, where you can make order and make charge. And admin-pages: where You can controll all data what are present in site.

Technologies:

 - bootstrap 4
 - Laravel 5.8
 - php 7.2
 - jquery 3.4.1

Installing:

- composer install
- php artisan key:generate
- php artisan migrate

Self artisan commands:
- register:admin (Create admin)
- register:super-admin (Create super admin)

Library for image
- lightgallery ( https://sachinchoolur.github.io/lightGallery/docs/ )
