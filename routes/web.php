<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('order')->group(function () {
    Route::post('pay', 'OrderController@pay')->name('order.pay');
    Route::get('', 'OrderController@index')->name('order');
    Route::get('images', 'OrderController@images')->name('order.images');
    Route::post('store', 'OrderController@store')->name('order.store');
    Route::get('refundView', 'AdminController@refundView')->name('order.refundView');
});

Auth::routes(['register' => false]);

Route::middleware(['auth'])->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('', 'AdminController@index')->name('admin');
        Route::get('changePrice', 'AdminController@changePrice')->name('admin.change_price');
        Route::post('storePass', 'AdminController@storePass')->name('admin.store_pass');
        Route::post('storePrice', 'AdminController@storePrice')->name('admin.store_price');
        Route::post('changeStatus{order_id?}', 'AdminController@changeStatus')->name('admin.changeStatus');
        Route::post('editPrice', 'AdminController@editPrice')->name('admin.editPrice');
        Route::get('orderDetails/{id}', 'AdminController@orderDetails')->name('admin.orderDetails');
        Route::get('paymentsErrors', 'AdminController@paymentsErrors')->name('admin.paymentsErrors');
        Route::get('refund', 'AdminController@refund')->name('admin.refund');
        Route::post('makeRefund', 'AdminController@makeRefund')->name('admin.makeRefund');
        Route::get('getForAjax', 'AdminController@getForAjax')->name('admin.getForAjax');
        Route::get('getForAjaxWithTrashed', 'AdminController@getForAjaxWithTrashed')->name('admin.getForAjaxWithTrashed');

        Route::prefix('super')->group(function () {
            Route::get('admin/{id?}', 'AdminController@superAdmin')->name('admin.super');
            Route::get('deleteUser/{id?}', 'AdminController@deleteUser')->name('deleteUser');
            Route::get('editUser/{id?}', 'AdminController@editUser')->name('editUser');
            Route::post('storeUser/{id?}', 'AdminController@storeUser')->name('storeUser');
            Route::get('createUser{id?}', 'AdminController@createUser')->name('createUser');
            Route::post('superAdminUser', 'AdminController@superAdminUser')->name('superAdminUser');
        });
    });
});
